//// Xx.

//// Xx.
import core from '../../core.mjs'
import OomBoxThing from './oom-box-thing.mjs'
import OomBoxListThing from './oom-box-list-thing.mjs'
import OomBoxRowThing from './oom-box-row-thing.mjs'

//// Create the singleton instances of the Thing Boxes.
new OomBoxThing(core)
new OomBoxListThing(core)
new OomBoxRowThing(core)
