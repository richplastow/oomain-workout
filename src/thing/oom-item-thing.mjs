//// A rather generic Oom Item.




//// DEPENDENCIES

import OomItem from '../../node_modules/oomkit/base/oom-item.mjs'
const IS_EDITABLE = 'IS_EDITABLE'




//// CLASS

export default class OomItemThing extends OomItem {




    //// API

    static get api () {
        return Object.assign({}, super.api, {
            flag: {
                default: false
              , IS_EDITABLE
            }
        })
    }




    //// CONSTRUCTOR

    constructor (custom={}) {
        super(custom)

        //// Record custom instantiation values, or fall back to defaults.
        const { flag } = custom
        const api = this.constructor.api
        this.flag = null == flag ? api.flag.default : flag

    }

}
