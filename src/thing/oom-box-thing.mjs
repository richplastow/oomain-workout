//// Xx.




//// DEPENDENCIES

import OomBox from '../../node_modules/oomkit/base/oom-box.mjs'




//// CLASS

export default class OomBoxThing extends OomBox {




    //// PROPERTIES

    get routes () { return [
        { match:/^list-things$/, tag:'list-things', title:'List Things' }
    ] }

    get renderables () { return [
        { match:/^list-things$|^home$/, tag:'thing-list', title:'Thing List', type:'vue' }
      , { tag:'thing-row', title:'Thing Row', type:'vue' }
    ] }

}
