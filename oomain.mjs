//// The main entry-point to this Oomain app.




//// DEPENDENCIES

//// Import general functionality from the Oomkit package.
import Vue from './node_modules/oomkit/vue.mjs' // vue.js converted to a module
import Dumpsta from './node_modules/oomkit/dumpsta.mjs'

//// Import core module instances.
import core from './core.mjs'
const { config, hub, state } = core

//// Import modules specific to this Oomain app.
import './src/thing/instantiate.mjs'




//// LIFECYCLE EVENTS

//// 1. After a Box has booted, check whether all Boxes have booted.
//// 2. When all Boxes have booted, define all their routes.
//// 3. After a Box has defined its routes, check whether all Boxes have.
//// 4. When all routes have been defined, define all renderable components.
//// 5. After a Box has defined its renderables, check whether all Boxes have.
//// 6. When all renderables have been defined, apply the current route.
hub.on('box-booted', onBoxLifecycleUpdate)
hub.on('box-all-booted', e => hub.fire('box-define-all-routes'))
hub.on('box-routes-defined', onBoxLifecycleUpdate)
hub.on('box-all-routes-defined', e => hub.fire('box-define-all-renderables'))
hub.on('box-renderables-defined', onBoxLifecycleUpdate)
hub.on('box-all-renderables-defined', e => {
    hub.fire('router-hashchange')
    startVue()
})

//// Tell every Box to run its `boot()` method, to kick off the whole shebang.
hub.fire('box-boot-all')




//// BOX LIFECYCLE EVENT HANDLERS

//// Handles events fired by Boxes when they complete a lifecycle-method.
//// If all Boxes have completed that lifecycle, it triggers the next stage.
function onBoxLifecycleUpdate (eventName, boxName) {
    const
        dev = config.data.dev
      , data = state.data
      , boxState = data[boxName]

    //// Do a little validation.
    if (null == boxState)
        throw Error(`Unexpected Box '${boxName}'`)
    if (eventName !== boxState.lifecycle) // eg, both are 'box-booted'
        throw Error(`'${boxName}' has inconsistent lifecycle state`)

    //// If Boxes are at different lifecycle stages, do nothing.
    for (const key in data) if (eventName !== data[key].lifecycle) return

    //// Signal that this lifecycle-stage has been completed by all Boxes.
    const done =
        'box-booted'              === eventName ? 'box-all-booted'
      : 'box-routes-defined'      === eventName ? 'box-all-routes-defined'
      : 'box-renderables-defined' === eventName ? 'box-all-renderables-defined'
      : `eventName '${eventName}' not recognised`
    if (1 < dev) console.log(`'${done}' x${Object.keys(data).length}`)
    hub.fire(done)
}


//// Creates the main Vue instance.
function startVue () {
    new Vue({
        el: '#oomain'
      , data: function () { return state.data }
      , computed: {
            routesMenu: function () {
                const router = state.data.OomBoxRouter
                return router.routes.filter( route => true )
            }
          , grid: function () {
                const router = state.data.OomBoxRouter
                return router.renderables.filter( renderable =>
                    renderable.match && renderable.match.test(router.route.tag))
            }
        }
    })
}

/*

//// Starts the main Dumpsta instance.
function startDumpsta () {
    new Dumpsta({
        data: function () { return {} }
    })
}

////
if ('object' === typeof document) {
    startVue()
} else {
    startDumpsta()
}
*/
