//// Xx.




//// Xx.
import OomHub from './node_modules/oomkit/core/oom-hub.mjs'
import OomBoxConfig from './node_modules/oomkit/core/oom-box-config.mjs'
import OomBoxRouter from './node_modules/oomkit/core/oom-box-router.mjs'
import OomBoxState from './node_modules/oomkit/core/oom-box-state.mjs'

//// Xx.
const
    tmpState  = { classTag:'oom-box-state' }  // prevent a constructor() Error
  , tmpConfig = { classTag:'oom-box-config' } // prevent a constructor() Error
  , hub = new OomHub()
  , state  = new OomBoxState( { config:tmpConfig, hub, state:tmpState })
  , config = new OomBoxConfig({ config:tmpConfig, hub, state })
  , router = new OomBoxRouter({ config          , hub, state })

//// Replace the placeholders with real core instances.
state.config = config
state.state = state
config.config = config

//// Xx.
export default { config, hub, router, state }
